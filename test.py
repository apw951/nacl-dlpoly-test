import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

def autocor(X, lags=1200):
    COR = np.zeros((lags,2))
    for i in tqdm(range(X.shape[0])):
        for t in range(lags):
            if i+t >= X.shape[0]:
                break
            COR[t,0] += X[i] * X[i+t] 
            COR[t,1] += 1
        
    return COR[:,0] / (1+COR[:,1])

fig, ax = plt.subplots()
ax.set_title("commit 2b50df16")

cors = []

for r in [1,2,4]:
    HF = np.loadtxt(f"{r}/HEATFLUX")[:,3:6]
    
    y = autocor(HF[:,0])
    x = np.linspace(0, y.shape[0], y.shape[0])
    cors.append((x,y))
    ax.scatter(x, y)

fig.savefig("NaCl-hfaf.png")

error = np.mean(np.sqrt((cors[0][1]-cors[1][1])**2))
print(f'Error {error}')
assert error < 1e-3
