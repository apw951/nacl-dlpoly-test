git clone https://gitlab.com/ccp5/dl-poly.git

cd dl-poly
rm -rf build
mkdir build
cd build
cmake .. -DBUILD_TESTING=OFF -DCMAKE_BUILD_TYPE=Release -DWITH_ASSERT=OFF -DDEBUG=OFF -DWITH_EXP=OFF && make
cd ../../

for r in 1 2 4
do
	rm -rf $r
	mkdir $r
	cp NaCl.* $r/
	cd $r
	mpirun -np $r --use-hwthread-cpus --oversubscribe ../dl-poly/build/bin/DLPOLY.Z -c NaCl.control
	cd ..
done

python3 test.py

